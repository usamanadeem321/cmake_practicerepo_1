#include <iostream>

int rect_area(int side1, int side2) { return side1 * side2; }
int circle_area(int r) { return 3.14 * r * r; }
